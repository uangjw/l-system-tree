#ifndef SCENE_H
#define SCENE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "camera.h"
#include "box.h"
#include "floor.h"
#include "skybox.h"
#include "light_box.h"
#include "directional_light.h"

class Scene {
public:
	Scene();
	Scene(glm::vec3 cameraPos);

	~Scene();

	void render(glm::mat4 model, glm::mat4& view, glm::mat4& proj);

private:
	//Skybox* skybox;
	Floor* floor;
	Box* box;
	//LightBox* lightbox;

	DirLight dirLight;

	glm::vec3 viewPos;
};

Scene::Scene() {
	std::cout << "Cannot construct a scene without cameraPos! " << std::endl;
}

Scene::Scene(glm::vec3 cameraPos) {
	viewPos = cameraPos;
	box = new Box;
	floor = new Floor;
	//lightbox = new LightBox(glm::vec3(1.2f, 1.0f, 2.0f));
}

Scene::~Scene() {
	delete box;
	delete floor;
	//delete lightbox;
}

void Scene::render(glm::mat4 model, glm::mat4& view, glm::mat4& proj) {
	floor->render(model, view, proj, dirLight, viewPos);
	box->render(model, view, proj, dirLight, viewPos);
	//lightbox->render(view, proj);
}

#endif
