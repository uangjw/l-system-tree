#ifndef SCENE_TREE_H
#define SCENE_TREE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>

#include "glm/glm.hpp"

#include "Terrain.h"
#include "camera.h"
#include "box.h"
#include "floor.h"
#include "skybox.h"
#include "directional_light.h"
#include "atree.h"


#define SHADOW_WIDTH 8192
#define SHADOW_HEIGHT 8192

class Scene {
public:
	Scene();
	Scene(glm::vec3 cameraPos);

	~Scene();

	void render(glm::mat4 model, glm::mat4& view, glm::mat4& proj);

private:
	Skybox* skybox;
	Floor* floor;
	Box* box;
	Atree* tree;
	Atree* tree1;
	Terrain* terrain;
	//LightBox* lightbox;
	Shader* shadowShader;
	int treenum=0;
	DirLight dirLight;
	GLuint depthMapFBO, depthMap;

	glm::vec3 viewPos;
};

Scene::Scene() {
	std::cout << "Cannot construct a scene without cameraPos! " << std::endl;
}

Scene::Scene(glm::vec3 cameraPos) {
	viewPos = cameraPos;
	box = new Box;
	floor = new Floor;
	tree = new Atree;
	tree1 = new Atree;
	skybox = new Skybox;
	terrain = new Terrain(string( "terrainTextures/Height.png"));

	//lightbox = new LightBox(glm::vec3(1.2f, 1.0f, 2.0f));

	glGenFramebuffers(1, &depthMapFBO);
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "????" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//terrain->setLightSpace(lightSpaceMatrix);

	shadowShader = new Shader(std::string("shaders/shadow.vert").c_str(), std::string("shaders/shadow.frag").c_str());
}

Scene::~Scene() {
	delete box;
	delete floor;
	delete tree;
	delete skybox;
	delete terrain;
}

void Scene::render(glm::mat4 model, glm::mat4& view, glm::mat4& proj) {
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	//首先生成深度贴图纹理depthMap（sammpler2D shadowMap）记得每个物体都调用rendershadow
	//floor->rendershadow(shadowShader);
	//box->rendershadow(shadowShader);
	treenum = 0;
	tree->rendershadow(shadowShader,treenum);
	terrain->renderShadow(shadowShader);
	//treenum = 1;
	//tree1->rendershadow(shadowShader,treenum);
	treenum = 0;
	//glBindVertexArray(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 2400, 1800);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	terrain->render(model, view, proj, depthMap);

	//floor->render(model, view, proj, dirLight, viewPos, depthMap);
	//box->render(model, view, proj, dirLight, viewPos);
	tree->render(model, view, proj, dirLight, viewPos, depthMap,treenum);
	treenum = 1;
	//tree1->render(model, view, proj, dirLight, viewPos, depthMap,treenum);
	skybox->render(view, proj);
}

#endif
