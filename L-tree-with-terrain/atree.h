#pragma once

#include <string>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "camera.h"
#include "directional_light.h"
#include "shader_s.h"

#include"tree.h"
#include"geo_tree.h"

mat4 RotateArbitraryLine(float x, float y, float d) {
	mat4 tmp = mat4(1.0f);
	tmp = translate(mat4(1.0f), vec3(-1 * x, -1 * y, 0));
	tmp = rotate(mat4(1.0f), radians(d), vec3(0.0f, 0.0f, 1.0f)) * tmp;
	tmp = translate(mat4(1.0f), vec3(x, y, 0)) * tmp;

	return tmp;
}

class Atree {
public:
	/*
	准备工作放在构造函数中
	渲染工作放在rander函数中
	*/
	Atree() {
		loops = 0;
		ourShader = new Shader("L-TreeShaders/tree_vs.txt", "L-TreeShaders/trunk_fs.txt");
		ourShader1 = new Shader("L-TreeShaders/tree_vs.txt", "L-TreeShaders/leaves_fs.txt");
		str="F[^$F[^%F[^$F[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]]\
					[/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]][*%FF[^%F[^$X]\
					[*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X]\
					[*%FX][&%X]]][&%F[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F\
					[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]][&$F[^$F[&%F[^$X][*%FX]\
					[&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX]\
					[&%X]]][*%FF[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X]\
					[*%FX][&%X]][*%F[^$X][*%FX][&%X]]][&%F[&%F[^$X][*%FX][&%X]][*$F\
					[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]][/$F\
					[^$F[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]]\
					[*%F[^$X][*%FX][&%X]]][*%FF[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]]\
					[/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]][&%F[&%F[^$X][*%FX][&%X]]\
					[*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]][*%F\
					[^$F[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]]\
					[^%F[^$X][*%FX][&%X]]][*%FF[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]]\
					[/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]][&%F[^%F[^$X][*%FX][&%X]]\
					[&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]]]]\
					[*%F[&%F[^$F[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX]\
					[&%X]][^%F[^$X][*%FX][&%X]]][*%FF[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX]\
					[&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]][&%F[&%F[^$X][*%FX]\
					[&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]]\
					[*$F[^$F[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]]\
					[*%F[^$X][*%FX][&%X]]][*%FF[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F\
					[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]][&%F[&%F[^$X][*%FX][&%X]][*$F[^$X]\
					[*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]][/$F[^$F[&%F[^$X]\
					[*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]\
					[*%FF[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X]\
					[*%FX][&%X]]][&%F[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]]\
					[*%F[^$X][*%FX][&%X]]]][^%F[^$F[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]]\
					[/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]][*%FF[&%F[^$X][*%FX][&%X]][*$F\
					[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]][&%F[&%F[^$X]\
					[*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]]]\
					[&%F[&%F[^$F[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X]\
					[*%FX][&%X]]][*%FF[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X]\
					[*%FX][&%X]][*%F[^$X][*%FX][&%X]]][&%F[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]]\
					[/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]][*$F[^$F[^%F[^$X][*%FX][&%X]][&$F[^$X]\
					[*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]][*%FF[&%F[^$X][*%FX][&%X]]\
					[*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]][&%F[&%F[^$X][*%FX][&%X]]\
					[*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]][/$F[^$F[&%F[^$X][*%FX]\
					[&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]][*%FF[^%F[^$X]\
					[*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]][&%F[^%F\
					[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]]][^%F\
					[^$F[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]\
					[*%FF[&%F[^$X][*%FX][&%X]][*$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][^%F[^$X][*%FX][&%X]]]\
					[&%F[^%F[^$X][*%FX][&%X]][&$F[^$X][*%FX][&%X]][/$F[^$X][*%FX][&%X]][*%F[^$X][*%FX][&%X]]]]]";
		MyTree = new tree("T1",str);
		MyTree->generate_branches();
		MyGeo_tree = new geo_tree(*MyTree, 7, "G1");
		MyGeo_tree->gen_cylinders();
		MyGeo_tree->gen_texture_C();
		len = MyGeo_tree->get_len_C();
		MyGeo_tree->write_vertices_C(v);

		tree_leaves = MyGeo_tree->get_tree_leaves();
		l_num = tree_leaves.size();
		for (int i = 0;i < l_num;i++) {
			mat4 tmp = mat4(1.0f);
			//所有树叶的落叶信号
			fall_sgn.push_back(0);


			//计算，与get_trans_mat_P方法相同，不如直接把get_tans_mat函数设为public
			tmp = MyGeo_tree->get_trans_mat_P(tree_leaves[i]);
			//给model加缩放：
			//计算缩放矩阵：
			mat4 t_scale = mat4(1.0f);
			t_scale = scale(t_scale, vec3(tree_leaves[i].size, tree_leaves[i].size, tree_leaves[i].size));
			tmp = tmp * t_scale;
			//
			//（旋转已经在get_trans_mat_P中实现了）
			leaf_models.push_back(tmp);


			float x = tree_leaves[i].pos.x;
			float y = tree_leaves[i].pos.y;
			mat4 tmpr = RotateArbitraryLine(x + OFFSET, y + OFFSET, 1);
			leaf_rotation.push_back(tmpr);

		}

		/*float a_leaf[] = {0.5f,0.0f,0.0f,0.0f,1.0f,0.0f,1.0f,0.0f,
				0.5f,0.0f,1.0f,0.0f,1.0f,0.0f,1.0f,1.0f,
				-0.5f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,
				0.5f,0.0f,1.0f,0.0f,1.0f,0.0f,1.0f,1.0f,
				-0.5f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,
				-0.5f,0.0f,1.0f,0.0f,1.0f,0.0f,0.0f,1.0f };
*/
		float a_leaf[] = { 0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
	   0.2f,0.0f,0.12f, 0.0f,1.0f,0.0f, 0.7f,0.12f,
	   0.3f,0.0f,0.25f, 0.0f,1.0f,0.0f, 0.8f,0.25f,

	   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
	   0.3f,0.0f,0.25f, 0.0f,1.0f,0.0f, 0.8f,0.25f,
	   0.35f,0.0f,0.4f, 0.0f,1.0f,0.0f, 0.85f,0.4f,

	   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
	   0.35f,0.0f,0.4f, 0.0f,1.0f,0.0f, 0.85f,0.4f,
	   0.32f,0.0f,0.6f, 0.0f,1.0f,0.0f, 0.82f,0.6f,

	   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
	   0.32f,0.0f,0.6f, 0.0f,1.0f,0.0f, 0.82f,0.6f,
	   0.2f,0.0f,0.8f, 0.0f,1.0f,0.0f, 0.7f,0.8f,

	   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
	   0.2f,0.0f,0.8f, 0.0f,1.0f,0.0f, 0.7f,0.8f,
	   0.0f,0.0f,1.0f, 0.0f,1.0f,0.0f, 0.5f,1.0f,
			//------------------------
   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
  -0.2f,0.0f,0.12f, 0.0f,1.0f,0.0f, 0.3f,0.12f,
  -0.3f,0.0f,0.25f, 0.0f,1.0f,0.0f, 0.2f,0.25f,

   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
  -0.3f,0.0f,0.25f, 0.0f,1.0f,0.0f, 0.2f,0.25f,
  -0.35f,0.0f,0.4f, 0.0f,1.0f,0.0f, 0.15f,0.4f,

   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
  -0.35f,0.0f,0.4f, 0.0f,1.0f,0.0f, 0.15f,0.4f,
  -0.32f,0.0f,0.6f, 0.0f,1.0f,0.0f, 0.18f,0.6f,

   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
  -0.32f,0.0f,0.6f, 0.0f,1.0f,0.0f, 0.18f,0.6f,
  -0.2f,0.0f,0.8f, 0.0f,1.0f,0.0f, 0.3f,0.8f,

   0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f, 0.5f,0.0f,
  -0.2f,0.0f,0.8f, 0.0f,1.0f,0.0f, 0.3f,0.8f,
  -0.0f,0.0f,1.0f, 0.0f,1.0f,0.0f, 0.5f,1.0f,

		};


		down = mat4(1.0f);
		down = translate(down, vec3(0.0f, 0.0f, -0.025f));
		t = 0;


		glGenVertexArrays(2, VAO);//生成顶点数组对象
		glGenBuffers(2, VBO);//生成顶点缓冲对象

		glBindVertexArray(VAO[0]);//绑定

		glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);//绑定缓冲类型
		glBufferData(GL_ARRAY_BUFFER, len * sizeof(float), v, GL_STATIC_DRAW);//复制缓冲区到内存

		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);//链接顶点属性0
		glEnableVertexAttribArray(0);//启用顶点属性0

		// texture coord attribute
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));//链接顶点属性1
		glEnableVertexAttribArray(1);//启用顶点属性1，纹理坐标

		// normal vector attribute
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));//链接顶点属性0
		glEnableVertexAttribArray(2);//启用顶点属性2

		//----------------------单个叶片-----------------------------
		glBindVertexArray(VAO[1]);//绑定

		glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);//绑定缓冲类型
		glBufferData(GL_ARRAY_BUFFER, sizeof(a_leaf), a_leaf, GL_STATIC_DRAW);//复制缓冲区到内存

		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);//链接顶点属性0
		glEnableVertexAttribArray(0);//启用顶点属性0
		// texture coord attribute
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));//链接顶点属性1
		glEnableVertexAttribArray(1);//启用顶点属性1，纹理坐标
		// normal vector attribute
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));//链接顶点属性0
		glEnableVertexAttribArray(2);//启用顶点属性0


			// texture 1 树干纹理
			// ---------
		glGenTextures(1, &texture1);
		glBindTexture(GL_TEXTURE_2D, texture1);//绑定一个纹理
		// set the texture wrapping parameters 环绕方式
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters 过滤方式
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// load image, create texture and generate mipmaps
		int width, height, nrChannels;
		stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
		unsigned char* data = stbi_load("L-TreeTextures/bark3.jpg", &width, &height, &nrChannels, 0);//加载图片
		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);//释放图像内存

		// texture 2 黄叶纹理
	// ---------
		glGenTextures(1, &texture2);
		glBindTexture(GL_TEXTURE_2D, texture2);
		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// load image, create texture and generate mipmaps
		data = stbi_load("L-TreeTextures/myleafY.png", &width, &height, &nrChannels, 0);
		if (data)
		{
			// note that the awesomeface.png has transparency and thus an alpha channel, so make sure to tell OpenGL the data type is of GL_RGBA
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);


		// texture 3 绿叶纹理
		// ---------
		glGenTextures(1, &texture3);
		glBindTexture(GL_TEXTURE_2D, texture3);//绑定一个纹理
		// set the texture wrapping parameters 环绕方式
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters 过滤方式
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// load image, create texture and generate mipmaps
		data = stbi_load("L-TreeTextures/myleafG.png", &width, &height, &nrChannels, 0);//加载图片
		if (data)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else
		{
			std::cout << "Failed to load texture3" << std::endl;
		}

		stbi_image_free(data);//释放图像内存


		ourShader->use();
		ourShader->setInt("texture1", 0);
		ourShader1->use();
		ourShader1->setInt("texture2", 1);
		ourShader1->setInt("texture3", 2);


	}

	~Atree() {
		glDeleteBuffers(2, VBO);
		glDeleteVertexArrays(2, VAO);
		delete ourShader;
	}
	void rendershadow(Shader* shadowShader,int treenum) {
		loops++;

		shadowShader->use();
		glm::mat4 model = glm::mat4(1.0f);
		model = rotate(model, radians(float(-90)), vec3(1, 0, 0));
		model = scale(model, vec3(0.1, 0.1, 0.1));
		//model = translate(model, vec3(-60.0, -70.0, -6.0));
		//model = translate(model, vec3(0.0, 0.0, -6.0));

		if (treenum == 1) model = translate(model, vec3(6.0, 5.0, 0.0));
		shadowShader->setMat4("model", model);

		shadowShader->setMat4("lightSpaceMatrix", dirLight.getLightSpaceMatrix());
		glBindVertexArray(VAO[0]);
		glDrawArrays(GL_TRIANGLES, 0, len / 5);
		//glBindVertexArray(0);
		//渲染所有树叶
		if (loops <= 1000) {
			t = (float)loops / (float)1000;
		}
		mat4 growth = scale(mat4(1.0f), vec3(t, t, t));
		for (int i = 0; i < l_num; i++) {
			//cout << "rander leaf" << endl;
			shadowShader->setMat4("model", model * leaf_models[i] * growth);
			
			shadowShader->setMat4("lightSpaceMatrix", dirLight.getLightSpaceMatrix());
			if (fall_sgn[i] == 1) {
				vec3 now = leaf_models[i] * vec4(0.0f, 0.0f, 0.0f, 1.0f);
				if (now.z > 0) {
					leaf_models[i] = leaf_rotation[i] * down * leaf_models[i];
				}

			}
			glBindVertexArray(VAO[1]);
			glDrawArrays(GL_TRIANGLES, 0, 30);
			

		}
		glBindVertexArray(0);


	}

	void render(glm::mat4 model, glm::mat4 view, glm::mat4 projection, DirLight dirlight, glm::vec3 viewPos, GLuint& depthMap,int treenum) {
		//loops++;
		ourShader->use();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		ourShader->setFloat("material.shininess", 64.0f);
		ourShader->setInt("material.diffuse", 0);
		ourShader->setInt("shadowMap", 1);
		ourShader->setBool("shadows", 1);
		model = rotate(model, radians(float(-90)), vec3(1, 0, 0));
		model = scale(model, vec3(0.1, 0.1, 0.1));
		//model = translate(model, vec3(0.0, 0.0, -6.0));
		if (treenum == 1) model = translate(model, vec3(6.0, 5.0, 0.0));
		ourShader->setMat4("model", model);
		ourShader->setMat4("view", view);
		ourShader->setMat4("projection", projection);
		ourShader->setMat4("lightSpaceMatrix", dirlight.getLightSpaceMatrix());

		ourShader->setVec3("dirlight.direction", dirlight.getLightDir());
		ourShader->setFloat("dirlight.ambient", dirlight.getAmbient());
		ourShader->setVec3("dirlight.lightColor", dirlight.getLightColor());

		ourShader->setVec3("viewPos", viewPos);
		glBindVertexArray(VAO[0]);
		glDrawArrays(GL_TRIANGLES, 0, len / 5);

		ourShader1->use();
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, texture3);

		ourShader->setFloat("material.shininess", 64.0f);

		//ourShader1->setMat4("model", model);
		ourShader1->setMat4("view", view);
		ourShader1->setMat4("projection", projection);
		ourShader1->setMat4("lightSpaceMatrix", dirlight.getLightSpaceMatrix());

		ourShader1->setVec3("dirlight.direction", dirlight.getLightDir());
		ourShader1->setFloat("dirlight.ambient", dirlight.getAmbient());
		ourShader1->setVec3("dirlight.lightColor", dirlight.getLightColor());

		ourShader1->setVec3("viewPos", viewPos);

		//选取掉落的树叶,增加了掉落率
		if (loops % 5 == 0 && loops > 1200) {
			int make_fall = rand() % l_num;
			if (fall_sgn[make_fall] == 1) {
				fall_sgn[(make_fall + 1) % l_num] = FALL;
			}
			else {
				fall_sgn[make_fall] = FALL;
			}
		}

		//计算uniform
		float k = 1;
		if (CHANGE_COLOR) {
			k = 1 - loops * 0.0004f;
			if (k < 0) {
				k = 0;
			}
		}
		
		
		int vertexColorLocation = glGetUniformLocation(ourShader1->ID, "k");

		//计算t_scale
		if (loops <= 1000) {
			t = (float)loops / (float)1000;
		}
		mat4 growth = scale(mat4(1.0f), vec3(t, t, t));

		glUniform1f(vertexColorLocation, k);//更新uniform

		//渲染所有树叶
		for (int i = 0;i < l_num;i++) {
			//cout << "rander leaf" << endl;
			ourShader1->setMat4("model", model*leaf_models[i] * growth);
			glBindVertexArray(VAO[1]);
			glDrawArrays(GL_TRIANGLES, 0, 30);
			if (fall_sgn[i] == 1) {
				vec3 now = leaf_models[i] * vec4(0.0f, 0.0f, 0.0f, 1.0f);
				if (now.z > 0) {
					leaf_models[i] = leaf_rotation[i] * down * leaf_models[i];
				}

			}

		}
	}
	
private:
	/*
	需要的对象：
	string(树的生成字符串)
	tree（树类）指针
	geo_tree（几何树类）指针
	用于储存树干、树叶顶点的数组
	用于储存树叶信息的数组及长度 
	用于储存树叶model矩阵的数组
	数组的长度
	各种变换矩阵（下落、旋转）
	（rander函数可以先不接受外部的矩阵信息）
	落叶信号数组
	shander（3个着色器）
	vao/vbo（两个缓冲区）
	texture（3种纹理）
	
	*/

	DirLight dirLight;
	unsigned int VBO[2], VAO[2];
	string str;
	tree* MyTree;
	geo_tree* MyGeo_tree;
	float* v; //储存树干信息
	int len;//树干信息长度
	unsigned int texture1, texture2, texture3;
	Shader* ourShader, *ourShader1;
	
	vector<Leaf> tree_leaves;
	int l_num;
	//所有树叶的空间位置矩阵，有l_num个元素，每个元素都是mat4
	vector<mat4> leaf_models;
	vector<int> fall_sgn;
	vector<mat4> leaf_rotation;//每片树叶的旋转矩阵
	mat4 down;
	float t;
	int loops;



};