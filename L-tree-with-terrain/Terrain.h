#ifndef TERRAIN_H
#define TERRAIN_H


#pragma once
#include "shader_s.h"
#include "camera.h"
#include "texture.h"
#include "directional_light.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <string>
#include <stack>

using namespace std;

#ifndef PI
#define PI 3.1415926
#endif
#ifndef PI2
#define PI2 6.2831853
#endif

#define SHADOW_WIDTH 8192
#define SHADOW_HEIGHT 8192

const GLfloat screenWidth = 2400;
const GLfloat screenHeight = 1800;
const glm::vec3 lightPos(2.0f, 4.0f, -1.0f);
//const glm::vec3 sunPos(900, 900, 900);
const glm::vec3 ambientLight(0.3f);
const glm::vec3 diffuseLight(0.7f);

class Terrain
{
public:
	Terrain(string path);
	~Terrain();

	void LoadHeighmap(string& path);
	inline float GetTrueHeight(int i, int j);
	inline float GetHeight(float i, float j);
	void render(glm::mat4 model, glm::mat4 view, glm::mat4 projection, GLuint& depthMap);
	void renderShadow(Shader* shadowShader);
	void GenerateTerrain();
	float Travel(float i, float j);

private:
	DirLight dirLight;
	unsigned char* HeightData;
	int width, height, nrComponents, numVertex;
	float scale;
	GLuint terrainVBO, terrainVAO, terrainEBO;
	Shader* terrainShader;
	CTexture terrainTex[3];
	CTexture Normal;

	inline int GetIndex(int i, int j);
};


#include "Terrain.h"



Terrain::Terrain(string path)
{
	numVertex = 0;
	scale = 50.0f;
	LoadHeighmap(path);
	GenerateTerrain();
	terrainShader = new Shader(string("shaders/terrain.vs").c_str(),
		string("shaders/terrain_pcss.fs").c_str());
	terrainTex[0].loadTexture("terrainTextures/a0.bmp", true);
	//terrainTex[1].loadTexture(ShaderPath + "textures/floor/HighTile.bmp", true);
	//terrainTex[2].loadTexture("terrainTextures/lowestTile.bmp", true);
	Normal.loadTexture("terrainTextures/Normal.jpg", true);
	terrainShader->setInt("terrain1", 0);
	//terrainShader->setInt("terrain2", 1);
	//terrainShader->setInt("terrain3", 2);
	terrainShader->setInt("normal", 1);
	terrainShader->setVec3("dirLight.direction", glm::normalize(lightPos));
	terrainShader->setVec3("dirLight.ambient", ambientLight);
	terrainShader->setVec3("dirLight.diffuse", diffuseLight);
}

Terrain::~Terrain()
{
	glDeleteBuffers(1, &terrainVBO);
	glDeleteBuffers(1, &terrainEBO);
	glDeleteVertexArrays(1, &terrainVAO);
	if (!terrainShader) {
		delete terrainShader;
	}
	stbi_image_free(HeightData);
}

void Terrain::LoadHeighmap(string& path)
{
	HeightData = stbi_load(path.c_str(), &width, &height, &nrComponents, 0);
	if (!HeightData) {
		std::cout << "Failed to load the Height map at path:" << path << std::endl;
		return;
	}
}

float Terrain::GetTrueHeight(int i, int j)
{
	unsigned char tmp = HeightData[GetIndex(i, j)];
	int ret = (int)tmp;
	return (float)ret / 255.0f;

}


float Terrain::GetHeight(float i, float j)
{
	return GetTrueHeight(i, j) * scale;
}

void Terrain::render(glm::mat4 model, glm::mat4 view, glm::mat4 projection, GLuint& depthMap)
{
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	model = glm::scale(model, glm::vec3(0.09, 0.09, 0.09));
	model = glm::translate(model, glm::vec3(0.0, -15.0, 0.0));
	
	terrainShader->use();
	terrainShader->setInt("terrain1", 0);
	//terrainShader->setInt("terrain2", 1);
	terrainShader->setMat4("lightSpaceMatrix", dirLight.getLightSpaceMatrix());
	terrainShader->setMat4("model", model);
	//terrainShader->setMat4("m", model);
	//terrainShader->setInt("terrain3", 2);
	terrainShader->setInt("normal", 1);
	terrainShader->setMat4("view", view);
	terrainShader->setMat4("projection", projection);
	terrainShader->setInt("shadowMap", 2);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, terrainTex[0].textureID);
	//glBindTexture(GL_TEXTURE_2D, Normal.textureID);

	glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, depthMap);
    glBindTexture(GL_TEXTURE_2D, Normal.textureID);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	/*glBindTexture(GL_TEXTURE_2D, terrainTex[1].textureID);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, terrainTex[2].textureID);
	glActiveTexture(GL_TEXTURE3);
	
	glActiveTexture(GL_TEXTURE4);
	*/
	

	glBindVertexArray(terrainVAO);
	glDrawElements(GL_TRIANGLES, numVertex, GL_UNSIGNED_INT, 0);
	//glDrawArrays(GL_TRIANGLES, 0, numVertex);

	glBindVertexArray(0);
	glDisable(GL_CULL_FACE);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Terrain::renderShadow(Shader* shadowShader)
{
	shadowShader->use();
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::scale(model, glm::vec3(0.09, 0.09, 0.09));
	model = glm::translate(model, glm::vec3(0.0, -15.0, 0.0));
	shadowShader->setMat4("model", model);
	glm::mat4 biasMatrix{
	0.5,0.0,0.0,0.0,
	0.0,0.5,0.0,0.0,
	0.0,0.0,0.5,0.0,
	0.5,0.5,0.5,1.0
	};
	shadowShader->setMat4("lightSpaceMatrix", dirLight.getLightSpaceMatrix());

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glBindVertexArray(terrainVAO);
	//glDrawArrays(GL_TRIANGLES, 0, numVertex);

	glDrawElements(GL_TRIANGLES, numVertex, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glDisable(GL_CULL_FACE);
}

int Terrain::GetIndex(int i, int j)
{
	return (i * width + j) * nrComponents;
}

void Terrain::GenerateTerrain()
{
	vector<glm::vec3>vertices;
	vector<int> indices;
	float w2 = width / 2.0f;//高度图的大小
	float z2 = height / 2.0f;
	vertices.reserve(width * height);
	indices.reserve(width * height * 4);
	for (int x = 0; x < height; x++) {
		for (int y = 0; y < width; y++) {
			glm::vec3 record;
			record.x = y -w2;
			record.z = x -z2;
			record.y = GetHeight(x, y);
			vertices.push_back(record);
		}
	}
	/**/
	for (int x = 0; x < height - 1; x++) {
		for (int y = 0; y < width - 1; y++) {
			indices.push_back(x * width + y);
			indices.push_back((x + 1) * width + y);
			indices.push_back((x + 1) * width + y + 1);

			indices.push_back(x * width + y);
			indices.push_back((x + 1) * width + y + 1);
			indices.push_back(x * width + y + 1);
		}
	}
	numVertex=indices.size();
	//numVertex = vertices.size();
	glGenBuffers(1, &terrainVBO);
	glGenBuffers(1, &terrainEBO);
	glGenVertexArrays(1, &terrainVAO);
	glBindVertexArray(terrainVAO);

	glBindBuffer(GL_ARRAY_BUFFER, terrainVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, terrainEBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), &indices[0], GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

float Terrain::Travel(float i, float j)//根据坐标找到对应的高度，让树从地面长出来而不是悬空
{
	int is = i + 512;
	int js = j + 512;
	if (is >= 1020)is = 1020;
	else if (is <= 10)is = 10;
	if (js >= 1020)js = 1020;
	else if (js <= 10)js = 10;
	float ret = GetHeight(is, js);

	//cout << is << js << "->" << ret << endl;
	return ret;
}

#endif TERRAIN_H

