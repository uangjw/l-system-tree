#ifndef SCENE_H
#define SCENE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>
//#include <GL/glew.h>
#include <glad/glad.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "camera.h"
#include "box.h"
#include "floor.h"
#include "skybox.h"
//#include "light_box.h"
#include "directional_light.h"




#define SHADOW_WIDTH 1024
#define SHADOW_HEIGHT 1024
class Scene {
public:
	Scene();
	Scene(glm::vec3 cameraPos);

	~Scene();

	void render(glm::mat4 model, glm::mat4& view, glm::mat4& proj);

private:
	//Skybox* skybox;
	Floor* floor;
	Box* box;
	//LightBox* lightbox;
	Shader* shadowShader;
	DirLight dirLight;
	GLuint depthMapFBO, depthMap;
	glm::vec3 viewPos;
};

Scene::Scene() {
	std::cout << "Cannot construct a scene without cameraPos! " << std::endl;
}

Scene::Scene(glm::vec3 cameraPos) {
	viewPos = cameraPos;
	box = new Box;
	floor = new Floor;
	//lightbox = new LightBox(glm::vec3(1.2f, 1.0f, 2.0f));
	//shadow

	
	glGenFramebuffers(1, &depthMapFBO);
    glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "????" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
    //shadow着色器作用：变换到光空间
	shadowShader = new Shader(std::string("shaders/shadow.vert").c_str(), std::string("shaders/shadow.frag").c_str());
	/*shadowShader->use();
	shadowShader->setMat4("lightSpaceMatrix", dirLight.getLightSpaceMatrix());
	glm::mat4 model = glm::mat4(1.0f);
	shadowShader->setMat4("model", model);*/

}

Scene::~Scene() {
	delete box;
	delete floor;
	//delete lightbox;
}

void Scene::render(glm::mat4 model, glm::mat4& view, glm::mat4& proj) {
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	//首先生成深度贴图纹理depthMap（sammpler2D shadowMap）记得每个物体都调用rendershadow
	floor->rendershadow(shadowShader);
	box->rendershadow(shadowShader);
	//glBindVertexArray(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 800, 600);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//会被遮挡的地板根据depthMap改写，没被遮挡的box可以不用改写
	floor->render(model, view, proj, dirLight, viewPos, depthMap);
	box->render(model, view, proj, dirLight, viewPos, depthMap);
	//lightbox->render(view, proj);
}


#endif
