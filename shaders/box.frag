#version 330 core
out vec4 FragColor;

struct Material {
    sampler2D diffuse;
    float shininess;
};

struct DirLight {
    vec3 direction;
    float ambient;
    vec3 lightColor;
};

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoord;
    vec4 FragPosLightSpace;
}fs_in;

uniform Material material;
uniform vec3 viewPos;
uniform DirLight dirlight;

void main() {
    vec3 color = texture(material.diffuse, fs_in.TexCoord).rgb;

    vec3 ambient = dirlight.ambient * color;

    vec3 norm = normalize(fs_in.Normal);
    vec3 lightDir = normalize(-dirlight.direction);

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * color;

    vec3 viewDir = normalize(viewPos - fs_in.FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);
    spec = pow(max(dot(norm, halfwayDir), 0.0), material.shininess);
    vec3 specular = spec * dirlight.lightColor;
    
    vec3 lighting = ambient + diffuse + specular;

    FragColor = vec4(lighting, 1.0f);
}