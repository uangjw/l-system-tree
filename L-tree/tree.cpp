#pragma once
#include"tree.h"



tree::tree() {
	//参数初始化
	name = "";
	tree_string = "";
	//level_num = level;
	main_trunk_height = MAIN_TRUNK_HEIGHT;
	main_trunk_radious = MAIN_TRUNK_RADIOUS;
	l_decay_co = L_DECAY_CO;
	r_decay_co = R_DECAY_CO;
	k = K;
	//状态初始化
	sgn = 1;//表示还未调用generate函数
	curr_state.dir = vec3(0, 0, 1);
	curr_state.pos = vec3(0, 0, 0);
	curr_state.length = main_trunk_height;
	curr_state.radious = main_trunk_radious;
	curr_state.level = 1;
}

tree::tree(string n,string str) {
	//参数初始化
	name = n;
	tree_string = str;
	//level_num = level;
	main_trunk_height = MAIN_TRUNK_HEIGHT;
	main_trunk_radious = MAIN_TRUNK_RADIOUS;
	l_decay_co = L_DECAY_CO;
	r_decay_co = R_DECAY_CO;
	k = K;
	//状态初始化
	sgn = 1;//表示还未调用generate函数
	curr_state.dir = vec3(0, 0, 1);
	curr_state.pos = vec3(0, 0, 0);
	curr_state.length = main_trunk_height;
	curr_state.radious = main_trunk_radious;
	curr_state.level = 1;
}

tree::~tree(){}

void tree::show_tree_info(){
	if (name.empty()) {
		cout << "Empty tree." << endl;
		return;
	}
	cout << "-----Tree " << name << " info:-----" << endl;
	cout << "Tree string: " << tree_string << endl;
	cout << "Main trunk height: " << main_trunk_height << endl;
	cout << "Length decay: " << l_decay_co << endl;
	cout << "Radious decay: " << r_decay_co << endl;
	cout << "k:" << k << endl;
	cout << "Current number of trunks: " << branches.size() << endl;
	cout << "Current number of leaves: " << leaves.size() << endl;
	if (sgn == 1) {
		cout << "Note: string updated but branches not" << endl;
	}
	cout << endl;
}

void tree::set_string(string str) {
	tree_string = str;
	sgn = 1;
	cout << "String reseted to: " << tree_string << endl;
}

void tree::set_name(string n) {
	name = n;
}

void tree::clear_states() {
	while (!states.empty()) {
		states.pop();
	}
	cout << "States cleared." << endl;
}

//注意以下两个函数并没有回收空间：
void tree::clear_branches() {
	branches.clear();
	cout << "Branches cleared." << endl;
}

void tree::clear_leaves() {
	leaves.clear();
	cout << "Leaves cleared." << endl;
}

void tree::generate_branches() {
	srand(time(NULL));
	clear_states();
	clear_branches();
	clear_leaves();
	cout << "Start to generate." << endl;
	for (size_t i = 0; i < tree_string.size(); i++) {
		char ch = tree_string[i];
		Trunk tmp;
		switch (ch) {
		case 'F': {

			float scale = 1;
			if (RANDOM) {
				int i = rand() % 40;
				scale += float(i - 20) / float(100);
			}

			tmp.len = curr_state.length * scale;
			tmp.begin = curr_state.pos;
			curr_state.pos += curr_state.dir * curr_state.length * scale;
			tmp.end = curr_state.pos;
			tmp.r = curr_state.radious;
			tmp.level = curr_state.level;
			branches.push_back(tmp);
			break;
		}
		case '$': {//y
			mat4 trans = mat4(1.0f);
			trans = rotate(trans, radians(DY), glm::vec3(0.0, 1.0, 0.0));
			vec4 tmp_dir = vec4(curr_state.dir, 1.0f);
			tmp_dir = trans * tmp_dir;
			curr_state.dir = vec3(tmp_dir.x, tmp_dir.y, tmp_dir.z);
			break;
		}	
		case '%': {
			mat4 trans = mat4(1.0f);
			trans = rotate(trans, radians(-DY), glm::vec3(0.0, 1.0, 0.0));
			vec4 tmp_dir = vec4(curr_state.dir, 1.0f);
			tmp_dir = trans * tmp_dir;
			curr_state.dir = vec3(tmp_dir.x, tmp_dir.y, tmp_dir.z);
			break;
		}
		case '^': {//x
			mat4 trans = mat4(1.0f);
			trans = rotate(trans, radians(DX), glm::vec3(1.0, 0.0, 0.0));
			vec4 tmp_dir = vec4(curr_state.dir, 1.0f);
			tmp_dir = trans * tmp_dir;
			curr_state.dir = vec3(tmp_dir.x, tmp_dir.y, tmp_dir.z);
			break;
		}
		case '&': {
			mat4 trans = mat4(1.0f);
			trans = rotate(trans, radians(-DX), glm::vec3(1.0, 0.0, 0.0));
			vec4 tmp_dir = vec4(curr_state.dir, 1.0f);
			tmp_dir = trans * tmp_dir;
			curr_state.dir = vec3(tmp_dir.x, tmp_dir.y, tmp_dir.z);
			break;
		}
		case '*': {//z
			mat4 trans = mat4(1.0f);
			trans = rotate(trans, radians(DZ), glm::vec3(0.0, 0.0, 1.0));
			vec4 tmp_dir = vec4(curr_state.dir, 1.0f);
			tmp_dir = trans * tmp_dir;
			curr_state.dir = vec3(tmp_dir.x, tmp_dir.y, tmp_dir.z);
			break;
		}
		case '/': {
			mat4 trans = mat4(1.0f);
			trans = rotate(trans, radians(-DZ), glm::vec3(0.0, 0.0, 1.0));
			vec4 tmp_dir = vec4(curr_state.dir, 1.0f);
			tmp_dir = trans * tmp_dir;
			curr_state.dir = vec3(tmp_dir.x, tmp_dir.y, tmp_dir.z);
			break;
		}
		case '[': {
			states.push(curr_state);
			curr_state.length *= l_decay_co;
			curr_state.radious *= r_decay_co;
			curr_state.level += 1;
			break;
		}
		
		case ']': {
			curr_state = states.top();
			states.pop();
			break;
		}

		case 'X': {
			Trunk tm = branches[branches.size() - 1];
			Leaf rs;
			rs.dir = curr_state.dir;
			rs.pos = tm.end;
			rs.size = SIZE;
			if (RANDOM) {
				int i = rand() % 80;
				rs.size += float(i-40) / float(100);
			}

			leaves.push_back(rs);
		}

		default: {
			break;
		}

		}
	}
	sgn = 0;
}

void tree::show_branches() {
	cout << "SHOW BRANCHES OF "<<name<< endl;
	if (!branches.empty()) {
		for (int i = 0;i < branches.size();i++) {
			cout << "----------" << endl;
			cout << "Trunk " << i << ":" << endl;
			print_trunk(branches[i]);
		}
	}
	else {
		cout << "No trunk.";
	}
	cout << endl;
}

void tree::print_trunk(Trunk T) {
	cout << "--begin: (" << T.begin.x\
		<< ", " << T.begin.y\
		<< ", " << T.begin.z<<")";
	cout<< "  end: (" << T.end.x\
		<<", "<<T.end.y\
		<<", "<<T.end.z<<")"<<endl;
	cout << "--level: " << T.level << endl;
	cout << "--Radious:" << T.r << endl;
}

void tree::print_leaf(Leaf L) {
	cout << "--pos: (" << L.pos.x\
		<< ", " << L.pos.y\
		<< ", " << L.pos.z << ")";
	cout << "  dir: (" << L.dir.x\
		<< ", " << L.dir.y\
		<< ", " << L.dir.z <<")"<< endl;
	cout << "size: " << L.size << endl;
}

void tree::show_leaves() {
	cout << "SHOW LEAVES OF " << name << ": "<<endl;
	if (!leaves.empty()) {
		for (int i = 0;i < leaves.size();i++) {
			cout << "----------" << endl;
			cout << "Leaf " << i << ":" << endl;
			print_leaf(leaves[i]);
		}
	}
	else {
		cout << "No leaf.";
	}
	cout << endl;
}

string tree::getname() {
	return name;
}

vector<Trunk> tree::getbranches() {
	return branches;
}

float tree::getk() {
	return k;
}

vector<Leaf> tree::getleaves() {
	return leaves;
}

