#ifndef DIRECTIONAL_LIGHT_H
#define DIRECTIONAL_LIGHT_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

class DirLight {
private:
	glm::vec3 lightDir;
	glm::mat4 lightSpaceMatrix;
	float ambient;
	glm::vec3 lightColor;

public:
	DirLight();
	glm::vec3 getLightDir() { return lightDir; }
	glm::mat4 getLightSpaceMatrix() { return lightSpaceMatrix; }
	float getAmbient() { return ambient; }
	glm::vec3 getLightColor() { return lightColor; }
};

DirLight::DirLight() {
	lightDir = glm::vec3(-2.0f, -4.0f, 1.0f);
	glm::vec3 lightPos = -lightDir;

	GLfloat near_plane = 1.0f, far_plane = 7.5f;
	glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	glm::mat4 lightView = glm::lookAt(lightPos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	lightSpaceMatrix = lightProjection * lightView;


	lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
	ambient = 0.3;
}

#endif
