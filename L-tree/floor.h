#ifndef FLOOR_H
#define FLOOR_H

#include <string>
#include "glm/glm.hpp"
#include "camera.h"
#include "directional_light.h"

class Floor {
private:
	GLuint VBO, VAO;
	Shader* shader;
	Shader* shadershadow;
	DirLight dirLight;
	GLuint textureID;
public:
	Floor();
	~Floor();
	void rendershadow(Shader* shadowShader);
	void render(glm::mat4 model, glm::mat4 view, glm::mat4 projection, DirLight dirlight, glm::vec3 viewPos, GLuint& depthMap);
};

Floor::Floor() {
	float vertices[] = {
		-25.0f, 0.0f, -25.0f,  0.0f, 1.0f, 0.0f,
		 25.0f, 0.0f, -25.0f,  0.0f, 1.0f, 0.0f,
		 25.0f, 0.0f,  25.0f,  0.0f, 1.0f, 0.0f,
		 25.0f, 0.0f,  25.0f,  0.0f, 1.0f, 0.0f,
		-25.0f, 0.0f,  25.0f,  0.0f, 1.0f, 0.0f,
		-25.0f, 0.0f, -25.0f,  0.0f, 1.0f, 0.0f
	};
	shader = new Shader(std::string("shaders/floor.vert").c_str(), std::string("shaders/floor_pcss.frag").c_str());
	shader->use();

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
}

Floor::~Floor() {
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
	delete shader;
}
void Floor::rendershadow(Shader* shadowShader) {

	shadowShader->use();
	glm::mat4 model = glm::mat4(1.0f);
	//model = glm::scale(model, glm::vec3(5.0f, 5.0f, 5.0f));
	//model = glm::translate(model, glm::vec3(0.0f, -0.5f, 0.0f));

	shadowShader->setMat4("model", model);
	shadowShader->setMat4("lightSpaceMatrix", dirLight.getLightSpaceMatrix());
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);

}
void Floor::render(glm::mat4 model, glm::mat4 view, glm::mat4 projection, DirLight dirlight, glm::vec3 viewPos, GLuint& depthMap) {
	//���������
	shader->use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	shader->setInt("shadowMap", 0);
	shader->setFloat("material.shininess", 10.0f);
	//model = glm::translate(model, glm::vec3(0.0f, -0.5f, 0.0f));
	//model = glm::scale(model, glm::vec3(5.0f, 5.0f, 5.0f));

	shader->setMat4("model", model);

	shader->setMat4("view", view);
	shader->setMat4("projection", projection);


	shader->setMat4("lightSpaceMatrix", dirlight.getLightSpaceMatrix());
	shader->setBool("shadows", 1);
	shader->setVec3("dirlight.direction", dirlight.getLightDir());
	shader->setFloat("dirlight.ambient", dirlight.getAmbient());
	shader->setVec3("dirlight.lightColor", dirlight.getLightColor());

	shader->setVec3("viewPos", viewPos);

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}
#endif
