#pragma once
#include<iostream>
#include<queue>
#include<stack>
#include<string>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include"tree.h"




using namespace std;
using namespace glm;

typedef vector<vec3> Cylinder;
typedef vector<vec3> Piece;

class geo_tree {
public:
	geo_tree(tree T, int slice, string n);
	~geo_tree();

	//调试用信息
	void show_geo_tree_info();
	void show_cylinders();
	void show_pieces();

	void gen_cylinders();     //生成圆柱信息
	void gen_pieces();		  //生成叶片信息



	void write_vertices_C(float*& vertices);//写树干的顶点数组
	void write_vertices_P(float*& vertices);//写树叶的顶点数组
	int get_len_C(); //获取树干数组的长度（float变量数，等于树干顶点数*5）
	int get_len_P(); //获取树叶数组的长度（float变量数，等于树叶顶点数*5）

	void gen_texture_C();       //生成树干的纹理坐标,
								//默认纹理是[0,1]*[0,1]的图像，
								//仅依据slice就可以确定每点的纹理坐标

	int get_pieces_num();       //获取叶片数量
	vector<Leaf> get_tree_leaves();

	mat4 get_trans_mat_C(Trunk Tr);//生成变换矩阵
	mat4 get_trans_mat_P(Leaf L);//生成变换矩阵

private:
	void print_cylinder(Cylinder C);
	void print_piece(Piece P);

	void make_trans_C(Cylinder& C, mat4 mat);
	void make_trans_P(Piece& P, mat4 mat);
	Piece draw_a_piece(Leaf L);//在原点画叶片
	Cylinder draw_a_cylinder(Trunk Tr);//在原点画圆柱


	void show_mat4(mat4 m);



	string name;
	tree T;
	int slice;
	vector<Cylinder> cylinders;//保存整棵树的圆柱信息，每个圆柱由多个三角形组成，每个圆柱带有法向量信息，向量的个数翻倍
	vector<Piece> pieces;	   //保存整棵树的叶片信息，每个叶片由两个三角形组成
	int point_num_C;
	int point_num_P;
	float k;

	vector<vec2> texture_C;
	vector<vec2> texture_P;

};