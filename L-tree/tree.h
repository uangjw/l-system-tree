#pragma once

#include<iostream>
#include<queue>
#include<stack>
#include<string>
#include<stdlib.h>
#include<time.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#define MAIN_TRUNK_HEIGHT 12    //主干长度
#define MAIN_TRUNK_RADIOUS 1	//主干半径
#define L_DECAY_CO 0.6          //长度衰减系数
#define R_DECAY_CO 0.55			//半径衰减系数
#define K 0.7					//树干锥形程度，1为正圆柱
#define SIZE 1.0                  //树叶大小
#define DY 25.0f					//绕Y轴旋转度数			
#define DX 25.0f					//绕X轴
#define DZ 25.0f					//绕Z轴
#define RANDOM 1                    //是否随机化
#define FALL 1						//是否落叶
#define CHANGE_COLOR 1              //是否变色
#define OFFSET 0.2                  //落叶旋转轴偏移

using namespace std;
using namespace glm;




/*
	树干类：
	保存一段树干的起点、终点、层数、长度、底面半径
*/
typedef struct Trunk {
	float len;	   //长度，用于gro_tree生成圆柱体
	vec3 begin;    //起点
	vec3 end;      //终点
	int level;	   //所在层数
	float r;       //底面半径（粗细）
}Trunk;



/*
	树叶类：
	保存一片树叶的位置、方向、大小
*/
typedef struct Leaf {
	vec3 pos;
	vec3 dir;
//  float rotation;  //在生成piece时先绕z轴旋转的角度
	float size;
}Leaf;



/*
	状态类：
	含有位置、方向、长度、半径、层次信息
*/
typedef struct State {
	vec3 pos;
	vec3 dir;
	float length;
	float radious;
	int level;
}State;




class tree {
	
public:
	tree();
	tree(string n,string str);   //构造函数，将其他变量初始化为默认值
	~tree();

	void set_string(string str); //设置树的字符串
	void set_name(string n);

	//调试用函数
	void show_tree_info();       //打印树的信息
	void show_branches();		 //打印所有枝条
	void show_leaves();			 //打印所有树叶

	void generate_branches();    //生成树枝和树叶，将它们存储在branches和leaves中

	//为了使geo_tree获取树的元素和性质
	string getname();
	vector<Trunk> getbranches(); 
	vector<Leaf> getleaves();
	float getk();


private:

	//生成前的清空工作
	void clear_states();
	void clear_branches();
	void clear_leaves();

	//调试用函数
	void print_trunk(Trunk T);
	void print_leaf(Leaf L);

	int sgn;                  //sgn为1，表示有未经生成的字符串
	string name;
	string tree_string;       //生成树的字符串
	float main_trunk_height;  //主干长度
	float main_trunk_radious; //主干半径
	float l_decay_co;         //每次迭代树干缩短的程度
	float r_decay_co;         //每次迭代树干变细的程度
	float k;                  //同一条树干上下粗细差距系数

	vector<Trunk> branches;     //保存所有的树干信息
	vector<Leaf> leaves;		//保存所有的树叶信息
	stack<State> states;        //状态栈
	State curr_state;           //当前状态
};