#include"geo_tree.h"

geo_tree::geo_tree(tree T, int slice, string n) {
	this->T = T;
	this->slice = slice;
	name = n;
	point_num_C = 0;
	point_num_P = 0;
	k = T.getk();

	texture_P.push_back(vec2(1, 0));
	texture_P.push_back(vec2(1, 1));
	texture_P.push_back(vec2(-1, 0));
	texture_P.push_back(vec2(1, 1));
	texture_P.push_back(vec2(-1, 1));
	texture_P.push_back(vec2(-1, 0));
}

geo_tree::~geo_tree() {

}

void geo_tree::show_geo_tree_info() {
	cout << "-----" << "GEO TREE: " << name << " INFO" << endl;
	cout << "From tree: " << T.getname() << endl;
}

void geo_tree::show_cylinders() {
	cout << "-----CYLINDERS OF " << name << "-----" << endl;
	for (int i = 0;i < cylinders.size();i++) {
		cout << i << "th cylinder " << endl;
		print_cylinder(cylinders[i]);
	}
}

void geo_tree::show_pieces() {
	cout << "-----CYLINDERS OF " << name << "-----" << endl;
	for (int i = 0;i < pieces.size();i++) {
		cout << i << "th piece " << endl;
		print_cylinder(pieces[i]);
	}
}



void geo_tree::gen_cylinders() {
	vector<Trunk> tmp_Tr = T.getbranches();
	for (int i = 0;i < tmp_Tr.size();i++) {
		Cylinder curr_cylinder=draw_a_cylinder(tmp_Tr[i]);
		mat4 curr_trans_mat = get_trans_mat_C(tmp_Tr[i]);
		make_trans_C(curr_cylinder, curr_trans_mat);
		cylinders.push_back(curr_cylinder);
	}
}


void geo_tree::gen_pieces() {
	vector<Leaf> tmp_L = T.getleaves();
	for (int i = 0;i < tmp_L.size();i++) {
		Piece curr_piece = draw_a_piece(tmp_L[i]);
		mat4 curr_trans_mat = get_trans_mat_P(tmp_L[i]);
		make_trans_C(curr_piece, curr_trans_mat);
		pieces.push_back(curr_piece);
	}
}

void geo_tree::write_vertices_C(float*& vertices) {
	int len = 5 * point_num_C;
	vertices = new float[len];
	int index = 0;
	for (int i = 0;i < cylinders.size();i++) {
		for (int j = 0;j < cylinders[i].size();j=j+2) {//改为j=j+2
			vertices[index++] = cylinders[i][j].x;
			vertices[index++] = cylinders[i][j].y;
			vertices[index++] = cylinders[i][j].z;

			vertices[index++] = cylinders[i][j+1].x;
			vertices[index++] = cylinders[i][j+1].y;
			vertices[index++] = cylinders[i][j+1].z;

			vertices[index++] = texture_C[j/2].x;
			vertices[index++] = texture_C[j/2].y;
		}
	}
}

void geo_tree::write_vertices_P(float*& vertices) {
	int len = 5 * point_num_P;
	vertices = new float[len];
	int index = 0;
	for (int i = 0;i < pieces.size();i++) {
		for (int j = 0;j < pieces[i].size();j++) {
			vertices[index++] = pieces[i][j].x;
			vertices[index++] = pieces[i][j].y;
			vertices[index++] = pieces[i][j].z;
			vertices[index++] = texture_P[j].x;
			vertices[index++] = texture_P[j].y;
		}
	}
}

int geo_tree::get_len_C() {
	return point_num_C * 5;
}
int geo_tree::get_len_P() {
	return point_num_P * 5;
}

void geo_tree::gen_texture_C() {
	for (int i = 0;i < slice - 1;i++) {
		float cor1x = float(i) / (float)(slice - 1.0f);
		float cor2x = float(i+1) / (float)(slice - 1.0f);

		vec2 point1 = vec2(cor1x, 1);
		vec2 point2 = vec2(cor1x, 0);
		vec2 point3 = vec2(cor2x, 1);
		vec2 point4 = vec2(cor2x, 0);

		//cout << point1.x << ", " << point1.y << endl;
		//cout << point2.x << ", " << point2.y << endl;
		//cout << point3.x << ", " << point3.y << endl;
		//cout << point4.x << ", " << point4.y << endl;

		texture_C.push_back(point1);
		texture_C.push_back(point2);
		texture_C.push_back(point3);
		texture_C.push_back(point3);
		texture_C.push_back(point2);
		texture_C.push_back(point4);

	}
}


int geo_tree::get_pieces_num() {
	return T.getleaves().size();
}

vector<Leaf> geo_tree::get_tree_leaves() {
	return T.getleaves();
}

mat4 geo_tree::get_trans_mat_C(Trunk Tr) {
	glm::mat4 trans(1.0f);
	trans = translate(trans, Tr.begin);
	vec3 z = vec3(0, 0, -1);
	vec3 p = Tr.begin - Tr.end;
	vec3 t = cross(z, p);
	float Pi = 3.1415926;
	float angle = acos(glm::dot(z, p) / length(p));
	mat4 rotas(1.0f);
	if (angle != 0) {
		rotas = rotate(rotas, angle, t);
	}
	mat4 rst = trans * rotas;
	return rst;
}

mat4 geo_tree::get_trans_mat_P(Leaf L) {
	glm::mat4 trans(1.0f);
	trans = translate(trans, L.pos);
	vec3 z = vec3(0, 0, 1);
	vec3 p = L.dir;
	vec3 t = cross(z, p);
	float Pi = 3.1415926;
	float angle = acos(glm::dot(z, p) / length(p));

	mat4 rotas(1.0f);
	if (RANDOM) {
		int i = rand() % 90;
		rotas = rotate(rotas, radians(float(i - 45)), vec3(0, 0, 1));
	}
	if (angle != 0) {
		rotas = rotate(rotas, angle, t);
	}
	mat4 rst = trans * rotas;
	return rst;
}


//----------------PRIVATE---------------



void geo_tree::print_cylinder(Cylinder C) {
	for (int i = 0;i < C.size();i++) {
		cout << "(" << C[i].x << ", " << C[i].y << ", " << C[i].z << ")" << endl;
	}
}



void geo_tree::print_piece(Piece P) {
	for (int i = 0;i < P.size();i++) {
		cout << "(" << P[i].x << ", " << P[i].y << ", " << P[i].z << ")" << endl;
	}
}

Cylinder geo_tree::draw_a_cylinder(Trunk Tr) {
	Cylinder tmp_cylinder; 

	float len = Tr.len;
	float radius = Tr.r;
	float delta = 360.0f / (float)(slice - 1.0);
	for (unsigned int x = 0;x < slice - 1;x++) {
		float angle = delta * x;
		float rc1 = radius * cos(glm::radians(angle));
		float rs1 = radius * sin(glm::radians(angle));
		float rc2 = radius * cos(glm::radians(angle + delta));
		float rs2 = radius * sin(glm::radians(angle + delta));

		glm::vec3 point1 = glm::vec3(k * rc1, k * rs1, len);
		glm::vec3 point2 = glm::vec3(rc1, rs1, 0.0);
		glm::vec3 point3 = glm::vec3(k * rc2, k * rs2, len);
		glm::vec3 point4 = glm::vec3(rc2, rs2, 0.0);


		glm::vec3 normal1 = point2 + glm::vec3(0, 0, radius * radius * (1 - k) / len);
		glm::vec3 normal2 = point4 + glm::vec3(0, 0, radius * radius * (1 - k) / len);


		//计算法向量（直接跟在每一个顶点信息后）只用两个点就够了

		tmp_cylinder.push_back(point1);
		tmp_cylinder.push_back(normal1);

		tmp_cylinder.push_back(point2);
		tmp_cylinder.push_back(normal1);

		tmp_cylinder.push_back(point3);
		tmp_cylinder.push_back(normal2);

		tmp_cylinder.push_back(point3);
		tmp_cylinder.push_back(normal2);

		tmp_cylinder.push_back(point2);
		tmp_cylinder.push_back(normal1);

		tmp_cylinder.push_back(point4);
		tmp_cylinder.push_back(normal2);

		//写入法向量

		point_num_C += 12;
		//改成+12
	}
	return tmp_cylinder;
}








void geo_tree::make_trans_C(Cylinder& C, mat4 mat) {
	vec4 out=vec4(0.0f);
	for (int i = 0;i < C.size();i++) {
		if (i % 2 == 0) {
			vec4 tmp = vec4(C[i], 1.0f);//点坐标变换
			out = mat * tmp;
		}
		else {
			vec4 tmp = vec4(C[i], 0.0f);//法向量变换
			out = mat * tmp;
		}
		
		C[i] = vec3(out.x, out.y, out.z);
	}
}




void geo_tree::make_trans_P(Piece& P, mat4 mat) {
	vec4 out = vec4(0.0f);
	for (int i = 0;i < P.size();i++) {
		if (i % 2 == 0) {
			vec4 tmp = vec4(P[i], 1.0f);//点坐标变换
			out = mat * tmp;
		}
		else {
			vec4 tmp = vec4(P[i], 0.0f);//法向量变换
			out = mat * tmp;
		}
		P[i] = vec3(out.x, out.y, out.z);
	}
}






Piece geo_tree::draw_a_piece(Leaf L) {
	Piece tmp_piece;
	float size = L.size;
	tmp_piece.push_back(vec3(size * 0.5, 0, 0));
	tmp_piece.push_back(vec3(size * 0.5, 0, size));
	tmp_piece.push_back(vec3(size * -0.5, 0, 0));
	tmp_piece.push_back(vec3(size * 0.5, 0, size));
	tmp_piece.push_back(vec3(size * -0.5, 0, size));
	tmp_piece.push_back(vec3(size * -0.5, 0, 0));
	point_num_P += 6;
	return tmp_piece;
}



void geo_tree::show_mat4(mat4 m) {
	for (int i = 0;i < 4;i++) {
		for (int j = 0;j < 4;j++) {
			cout << m[i][j] << " ";
		}
		cout << endl;
	}
}

